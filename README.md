## API de Gerenciamento de Blog e Newsletter

Esta API é projetada para gerenciar as postagens de um blog, uma newsletter e o gerenciamento de membros da equipe (staff). A API é construída utilizando TypeScript, PostgreSQL, Prisma ORM, JWT (JSON Web Token) e Express.

### Tecnologias Utilizadas

- **TypeScript**: Linguagem de programação usada para desenvolver a API.
- **PostgreSQL**: Banco de dados relacional utilizado para armazenar dados.
- **Prisma ORM**: Ferramenta ORM (Object-Relational Mapping) para interagir com o banco de dados PostgreSQL.
- **JWT** (JSON Web Token): Método para autenticação e autorização.
- **Express**: Framework para criação de APIs em Node.js.

### Autenticação

Para acessar as rotas protegidas, é necessário incluir um token JWT no cabeçalho de autorização. Exemplo:

```http
Authorization: Bearer seu_token_jwt
```

### Rotas da API

| Módulo | Método | Rota        | Requer Authorization |
| ------ | ------ | ----------- | -------------------- |
| POST   | POST   | /post       | ✅                   |
| POST   | GET    | /post      | ❌                   |
| POST   | GET    | /post/:id  | ❌                   |
| POST   | PUT    | /post/:id  | ✅                   |
| POST   | DELETE | /post/:id  | ✅                   |
| USER   | POST   | /user      | ❌                   |
| USER   | GET    | /user      | ✅                   |
| USER   | GET    | /user/:id  | ✅                   |
| USER   | PUT    | /user/:id  | ✅                   |
| USER   | DELETE | /user/:id  | ✅                   |
| STAFF  | POST   | /staff     | ✅                   |
| STAFF  | GET    | /staff     | ✅                   |
| STAFF  | GET    | /staff/:id | ✅                   |
| STAFF  | PUT    | /staff/:id | ✅                   |
| STAFF  | DELETE | /staff/:id | ✅                   |

#### Instalação

1. Clone o repositório:
   ```bash
    git clone https://gitlab.com/apis9835024/blog.git
   ```
2. Instale as dependências:
   ```bash
    cd blog
    npm install
   ```
3. Configure o banco de dados PostgreSQL e Prisma:
   - Atualize o arquivo .env com suas credenciais do banco de dados.
   - Execute o comando para gerar as tabelas com Prisma:
   ```bash
    npx prisma db push
   ```
4. Inicie o servidor:
   ```bash
    npm run dev
   ```

---

Feito com ❤️ por Dhomini.
