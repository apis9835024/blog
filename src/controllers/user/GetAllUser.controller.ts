import { Request, Response } from "express";
import { UserService } from "../../services/User.service";

export class GetAllUserController {
  async handle(req: Request, res: Response) {
    try {
      const result = await new UserService().getAll();

      return res.status(200).send(
        result.map((r) => {
          return { ...r, id: Number(r.id) };
        })
      );
    } catch (err) {
      console.log(err);
      return res.status(502).send({
        title: "Error",
        message: "Occurred an error to get all users",
      });
    }
  }
}
