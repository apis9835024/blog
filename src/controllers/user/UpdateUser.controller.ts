import { User } from "@prisma/client";
import { Request, Response } from "express";
import { UserService } from "../../services/User.service";

export class UpdateUserController {
  async handle(req: Request, res: Response) {
    try {
      const user: Partial<User> = req.body;
      const userId: number = Number(req.params["id"]);

      const result = await new UserService().update(userId, user);

      return res.status(200).send({ ...result, id: Number(result.id) });
    } catch (err) {
      return res.status(502).send({
        title: "Error",
        message: "Occurred an error to update a user",
      });
    }
  }
}
