import { User } from "@prisma/client";
import { Request, Response } from "express";
import { UserService } from "../../services/User.service";

export class CreateUserController {
  async handle(req: Request, res: Response) {
    try {
      const user: User = req.body;

      const result = await new UserService().create(user);

      return res.status(201).send({ ...result, id: Number(result.id) });
    } catch (err) {
      return res.status(502).send({
        title: "Error",
        message: "Occurred an error to create a user",
      });
    }
  }
}
