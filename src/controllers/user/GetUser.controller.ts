import { Request, Response } from "express";
import { UserService } from "../../services/User.service";

export class GetUserController {
  async handle(req: Request, res: Response) {
    try {
      const userId: number = Number(req.params["id"]);

      const result = await new UserService().get(userId);

      return res.status(200).send({ ...result, id: Number(result?.id) });
    } catch (err) {
      return res.status(502).send({
        title: "Error",
        message: "Occurred an error to get a user",
      });
    }
  }
}
