import { Staff } from "@prisma/client";
import { Request, Response } from "express";
import sha256 from "sha256";
import { StaffService } from "../../services/Staff.service";

export class CreateStaffController {
  async handle(req: Request, res: Response) {
    try {
      const staff: Staff = req.body;

      staff.password = sha256(staff.password, { asString: true });

      const result = await new StaffService().create(staff);

      return res.status(201).send({ ...result, id: Number(result.id) });
    } catch (err) {
      return res.status(502).send({
        title: "Error",
        message: "Occurred an error to create a staff",
      });
    }
  }
}
