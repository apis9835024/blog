import { Staff } from "@prisma/client";
import { Request, Response } from "express";
import { StaffService } from "../../services/Staff.service";

export class UpdateStaffController {
  async handle(req: Request, res: Response) {
    try {
      const staff: Partial<Staff> = req.body;
      const staffId: number = Number(req.params["id"]);

      const result = await new StaffService().update(staffId, staff);

      return res.status(200).send({ ...result, id: Number(result.id) });
    } catch (err) {
      return res.status(502).send({
        title: "Error",
        message: "Occurred an error to update a staff",
      });
    }
  }
}
