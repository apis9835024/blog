import { Request, Response } from "express";
import { StaffService } from "../../services/Staff.service";

export class GetAllStaffController {
  async handle(req: Request, res: Response) {
    try {
      const result = await new StaffService().getAll();

      return res.status(200).send(
        result.map((r) => {
          return { ...r, id: Number(r.id) };
        })
      );
    } catch (err) {
      return res.status(502).send({
        title: "Error",
        message: "Occurred an error to get all staffs",
      });
    }
  }
}
