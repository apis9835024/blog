import { Request, Response } from "express";
import { StaffService } from "../../services/Staff.service";

export class GetStaffController {
  async handle(req: Request, res: Response) {
    try {
      const staffId: number = Number(req.params["id"]);

      const result = await new StaffService().get(staffId);

      return res.status(200).send({ ...result, id: Number(result?.id) });
    } catch (err) {
      return res.status(502).send({
        title: "Error",
        message: "Occurred an error to get a staff",
      });
    }
  }
}
