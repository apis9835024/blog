import { Staff } from "@prisma/client";
import { Request, Response } from "express";
import sha256 from "sha256";
import { prisma } from "../configs/database";
import { JsonWebTokenService } from "../services/JsonWebToken.service";

export class LogInController {
  async handle(req: Request, res: Response) {
    try {
      const staff: Staff = req.body;

      staff.password = sha256(staff.password, { asString: true });

      const validateIdentify = await prisma.staff.findFirst({
        where: {
          email: staff.email,
          password: staff.password,
        },
        select: {
          email: true,
          name: true,
          id: true,
        },
      });

      if (!validateIdentify) {
        return res.status(401).send({
          title: "User unouthorized",
          message: "User is not authorized to access this resource",
        });
      }

      const token = await new JsonWebTokenService().createToken({
        ...validateIdentify,
        id: Number(validateIdentify.id),
      });

      if (token instanceof Error) {
        return res.status(401).send({
          title: "User unouthorized",
          message: "User is not authorized to access this resource",
        });
      }

      return res.status(200).send({ token: token });
    } catch (err) {
      console.log(err);
      return res.status(401).send({
        title: "User unouthorized",
        message: "User is not authorized to access this resource",
      });
    }
  }
}
