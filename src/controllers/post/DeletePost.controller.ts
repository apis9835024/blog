import { Request, Response } from "express";
import { PostService } from "../../services/Post.service";

export class DeletePostController {
  async handle(req: Request, res: Response) {
    try {
      const postId: string = req.params["id"];

      const result = await new PostService().delete(postId);

      return res.status(200).send(result);
    } catch (err) {
      return res.status(502).send({
        title: "Error",
        message: "Occurred an error to delete a post",
      });
    }
  }
}
