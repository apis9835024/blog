import { Post } from "@prisma/client";
import { Request, Response } from "express";
import { PostService } from "../../services/Post.service";

export class UpdatePostController {
  async handle(req: Request, res: Response) {
    try {
      const post: Partial<Post> = req.body;
      const postId: string = req.params["id"];

      const result = await new PostService().update(postId, post);

      return res.status(200).send(result);
    } catch (err) {
      return res.status(502).send({
        title: "Error",
        message: "Occurred an error to update a post",
      });
    }
  }
}
