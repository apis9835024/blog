import { Post } from "@prisma/client";
import { Request, Response } from "express";
import { PostService } from "../../services/Post.service";

export class CreatePostController {
  async handle(req: Request, res: Response) {
    try {
      const post: Post = req.body;

      const result = await new PostService().create(post);

      return res.status(201).send(result);
    } catch (err) {
      return res.status(502).send({
        title: "Error",
        message: "Occurred an error to create a post",
      });
    }
  }
}
