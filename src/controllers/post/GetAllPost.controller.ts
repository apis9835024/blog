import { Request, Response } from "express";
import { PostService } from "../../services/Post.service";

export class GetAllPostController {
  async handle(req: Request, res: Response) {
    try {
      const result = await new PostService().getAll();

      return res.status(200).send(result);
    } catch (err) {
      return res.status(502).send({
        title: "Error",
        message: "Occurred an error to get all posts",
      });
    }
  }
}
