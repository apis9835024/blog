import { Request, Response } from "express";
import { PostService } from "../../services/Post.service";

export class GetPostController {
  async handle(req: Request, res: Response) {
    try {
      const postId: string = req.params["slug"];

      const result = await new PostService().get(postId);

      return res.status(200).send(result);
    } catch (err) {
      return res.status(502).send({
        title: "Error",
        message: "Occurred an error to get a post",
      });
    }
  }
}
