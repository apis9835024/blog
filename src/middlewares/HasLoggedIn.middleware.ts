import { Staff } from "@prisma/client";
import { NextFunction, Request, Response } from "express";
import { JsonWebTokenService } from "../services/JsonWebToken.service";

declare global {
  namespace Express {
    interface Request {
      staff?: Partial<Staff>;
    }
  }
}

export class HasLoggedInMiddleware {
  async handle(req: Request, res: Response, next: NextFunction) {
    try {
      const _token = req.headers.authorization;

      if (!_token) {
        return res
          .status(401)
          .send({ title: "Invalid token", message: "Token was not sended" });
      }

      if (!_token.startsWith("Bearer ")) {
        return res.status(401).send({
          title: "Invalid token",
          message: "The Token sended is invalid",
        });
      }

      const token = _token.replace("Bearer ", "");

      const validate = await new JsonWebTokenService().validateToken(token);

      req.staff = validate;
      return next();
    } catch (err) {
      return res.status(401).send({
        title: "Invalid token",
        message: "The Token sended is invalid",
      });
    }
  }
}
