import { Staff } from "@prisma/client";
import jwt from "jsonwebtoken";

interface StaffPayload extends Omit<Staff, "id"> {
  id: number;
}

export class JsonWebTokenService {
  private PrivateKey: string = process.env.JWT_PRIVATE_KEY as unknown as string;

  createToken(
    staff: Partial<StaffPayload>
  ): Promise<string | undefined | Error> {
    return new Promise((resolve, reject) => {
      jwt.sign(
        {
          id: staff.id,
          name: staff.name,
          email: staff.email,
        },
        this.PrivateKey,
        { expiresIn: "1h" },
        (error, token) => {
          if (error) reject(error);
          else resolve(token);
        }
      );
    });
  }

  validateToken(token: string): Promise<Partial<Staff> | jwt.VerifyErrors> {
    return new Promise((resolve, reject) => {
      jwt.verify(token, this.PrivateKey, (error, decoded) => {
        if (error) reject(error);
        else resolve(decoded as Partial<Staff>);
      });
    });
  }
}
