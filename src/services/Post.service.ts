import { Post } from "@prisma/client";
import { prisma } from "../configs/database";

export class PostService {
  create(post: Post): Promise<Post> {
    return prisma.post.create({
      data: {
        title: post.title,
        slug: post.slug,
        content: post.content,
      },
      select: {
        id: true,
        title: true,
        slug: true,
        content: true,
        created_at: true,
      },
    });
  }

  get(slug: string): Promise<Post | null> {
    return prisma.post.findFirst({
      where: {
        slug: slug,
      },
      select: {
        id: true,
        title: true,
        slug: true,
        content: true,
        created_at: true,
      },
    });
  }

  getAll(): Promise<Post[]> {
    return prisma.post.findMany();
  }

  delete(id: string): Promise<Post> {
    return prisma.post.delete({
      where: {
        id: id,
      },
      select: {
        id: true,
        title: true,
        slug: true,
        content: true,
        created_at: true,
      },
    });
  }

  update(id: string, post: Partial<Post>): Promise<Post> {
    return prisma.post.update({
      data: post,
      where: {
        id: id,
      },
      select: {
        id: true,
        title: true,
        slug: true,
        content: true,
        created_at: true,
      },
    });
  }
}
