import { Staff } from "@prisma/client";
import { prisma } from "../configs/database";

export class StaffService {
  create(staff: Staff): Promise<Partial<Staff>> {
    return prisma.staff.create({
      data: {
        name: staff.name,
        email: staff.email,
        password: staff.password,
      },
      select: {
        id: true,
        name: true,
        email: true,
      },
    });
  }

  get(id: number): Promise<Partial<Staff> | null> {
    return prisma.staff.findFirst({
      where: {
        id: id,
      },
      select: {
        id: true,
        name: true,
        email: true,
      },
    });
  }

  getAll(): Promise<Partial<Staff>[]> {
    return prisma.staff.findMany({
      select: {
        id: true,
        name: true,
        email: true,
      },
    });
  }

  delete(id: number): Promise<Partial<Staff>> {
    return prisma.staff.delete({
      where: {
        id: id,
      },
      select: {
        id: true,
        name: true,
        email: true,
      },
    });
  }

  update(id: number, staff: Partial<Staff>): Promise<Partial<Staff>> {
    return prisma.staff.update({
      data: staff,
      where: {
        id: id,
      },
      select: {
        id: true,
        name: true,
        email: true,
      },
    });
  }
}
