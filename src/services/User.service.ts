import { User } from "@prisma/client";
import { prisma } from "../configs/database";

export class UserService {
  create(user: User): Promise<User> {
    return prisma.user.create({
      data: {
        name: user.name,
        email: user.email,
      },
      select: {
        id: true,
        name: true,
        email: true,
      },
    });
  }

  get(id: number): Promise<User | null> {
    return prisma.user.findFirst({
      where: {
        id: id,
      },
      select: {
        id: true,
        name: true,
        email: true,
      },
    });
  }

  getAll(): Promise<User[]> {
    return prisma.user.findMany();
  }

  delete(id: number): Promise<User> {
    return prisma.user.delete({
      where: {
        id: id,
      },
      select: {
        id: true,
        name: true,
        email: true,
      },
    });
  }

  update(id: number, user: Partial<User>): Promise<User> {
    return prisma.user.update({
      data: user,
      where: {
        id: id,
      },
      select: {
        id: true,
        name: true,
        email: true,
      },
    });
  }
}
