import { Router } from "express";
import { LogInController } from "../controllers/LogIn.controller";

export const LoginRouter = Router();

LoginRouter.post("/", new LogInController().handle);
