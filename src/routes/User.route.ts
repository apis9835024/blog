import { Router } from "express";
import { CreateUserController } from "../controllers/user/CreateUser.controller";
import { DeleteUserController } from "../controllers/user/DeletePostController";
import { GetAllUserController } from "../controllers/user/GetAllUser.controller";
import { GetUserController } from "../controllers/user/GetUser.controller";
import { UpdateUserController } from "../controllers/user/UpdateUser.controller";
import { HasLoggedInMiddleware } from "../middlewares/HasLoggedIn.middleware";

export const UserRouter = Router();

UserRouter.get(
  "/:id",
  new HasLoggedInMiddleware().handle,
  new GetUserController().handle
);

UserRouter.get(
  "/",
  new HasLoggedInMiddleware().handle,
  new GetAllUserController().handle
);

UserRouter.post("/", new CreateUserController().handle);

UserRouter.put(
  "/:id",
  new HasLoggedInMiddleware().handle,
  new UpdateUserController().handle
);

UserRouter.delete(
  "/:id",
  new HasLoggedInMiddleware().handle,
  new DeleteUserController().handle
);
