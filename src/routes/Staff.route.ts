import { Router } from "express";
import { CreateStaffController } from "../controllers/staff/CreateStaff.controller";
import { DeleteStaffController } from "../controllers/staff/DeleteStaff.controller";
import { GetAllStaffController } from "../controllers/staff/GetAllStaff.controller";
import { GetStaffController } from "../controllers/staff/GetStaff.controller";
import { UpdateStaffController } from "../controllers/staff/UpdateStaff.controller";
import { HasLoggedInMiddleware } from "../middlewares/HasLoggedIn.middleware";

export const StaffRouter = Router();

StaffRouter.post(
  "/",
  new HasLoggedInMiddleware().handle,
  new CreateStaffController().handle
);

StaffRouter.get(
  "/:id",
  new HasLoggedInMiddleware().handle,
  new GetStaffController().handle
);

StaffRouter.get(
  "/",
  new HasLoggedInMiddleware().handle,
  new GetAllStaffController().handle
);

StaffRouter.put(
  "/:id",
  new HasLoggedInMiddleware().handle,
  new UpdateStaffController().handle
);

StaffRouter.delete(
  "/:id",
  new HasLoggedInMiddleware().handle,
  new DeleteStaffController().handle
);
