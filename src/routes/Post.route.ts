import { Router } from "express";
import { CreatePostController } from "../controllers/post/CreatePost.controller";
import { DeletePostController } from "../controllers/post/DeletePost.controller";
import { GetAllPostController } from "../controllers/post/GetAllPost.controller";
import { GetPostController } from "../controllers/post/GetPost.controller";
import { UpdatePostController } from "../controllers/post/UpdatePost.controller";
import { HasLoggedInMiddleware } from "../middlewares/HasLoggedIn.middleware";

export const PostRouter = Router();

PostRouter.post(
  "/",
  new HasLoggedInMiddleware().handle,
  new CreatePostController().handle
);

PostRouter.get(
  "/:slug",
  new GetPostController().handle
);

PostRouter.get(
  "/",
  new GetAllPostController().handle
);

PostRouter.put(
  "/:id",
  new HasLoggedInMiddleware().handle,
  new UpdatePostController().handle
);

PostRouter.delete(
  "/:id",
  new HasLoggedInMiddleware().handle,
  new DeletePostController().handle
);
