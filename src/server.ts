import cors from "cors";
import express from "express";
import { LoginRouter } from "./routes/LogIn.route";
import { PostRouter } from "./routes/Post.route";
import { StaffRouter } from "./routes/Staff.route";
import { UserRouter } from "./routes/User.route";

const app = express();

app.use(cors({ origin: "*" }));
app.use(express.json());

app.use("/post", PostRouter);
app.use("/user", UserRouter);
app.use("/staff", StaffRouter);
app.use("/login", LoginRouter);

app.listen(3000, () => console.log("Server is listening..."));
